module Main where

import Prelude
import Effect (Effect)
import Data.Maybe (Maybe(..))
import Effect.Exception as Exception
import Web.DOM.NonElementParentNode (getElementById)
import Web.HTML (window)
import Web.HTML.HTMLDocument (toNonElementParentNode)
import Web.HTML.Window (document)
import React.Basic.DOM (render)
import Component.App (app)

main :: Effect Unit
main = do
  top <- document =<< window
  container <- getElementById "app" (toNonElementParentNode top)
  case container of
    Nothing -> Exception.throw "#app container element not found"
    Just el -> render (app { header: "puresweeper" }) el
