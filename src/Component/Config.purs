module Component.Config where

import Prelude
import Data.Int (fromString)
import Data.Maybe (Maybe(..))
import Effect (Effect)
import Effect.Unsafe (unsafePerformEffect)
import React.Basic.Hooks as React
import React.Basic.DOM as R
import React.Basic.Events as RE
import React.Basic.DOM.Events as RDE

type Props
  = { state :: ConfigState
    , setState :: ConfigState -> Effect Unit
    , onSubmit :: Unit -> Effect Unit
    }

config :: Props -> React.JSX
config = unsafePerformEffect mkConfig

newtype ConfigState
  = ConfigState
  { rows :: Int
  , cols :: Int
  , bombs :: Int
  }

initConfigState :: ConfigState
initConfigState =
  ConfigState
    { rows: 30
    , cols: 16
    , bombs: 99
    }

mkConfig :: React.Component Props
mkConfig = do
  React.component "Config" \props -> do
    let
      (ConfigState st) = props.state
    pure
      $ R.div
          { className: "config"
          , children:
              [ R.form
                  { onSubmit: RE.handler RDE.preventDefault (\_ -> props.onSubmit unit)
                  , children:
                      [ numberInput
                          { label: "Columns"
                          , id: "cols"
                          , onChange: \n -> props.setState (ConfigState st { cols = n })
                          , value: st.cols
                          , min: 5
                          , max: 100
                          }
                      , numberInput
                          { label: "Rows"
                          , id: "rows"
                          , onChange: \n -> props.setState (ConfigState st { rows = n })
                          , value: st.rows
                          , min: 5
                          , max: 100
                          }
                      , numberInput
                          { label: "Bombs"
                          , id: "bombs"
                          , onChange: \n -> props.setState (ConfigState st { bombs = n })
                          , value: st.bombs
                          , min: minBombs props.state
                          , max: maxBombs props.state
                          }
                      , R.input
                          { type: "submit"
                          , value: "Start"
                          }
                      ]
                  }
              ]
          }

maxBombs :: ConfigState -> Int
maxBombs (ConfigState st) = (cells * 9) / 10
  where
  cells = st.rows * st.cols

minBombs :: ConfigState -> Int
minBombs (ConfigState st) = cells / 10
  where
  cells = st.rows * st.cols

type NumberInputProps
  = { label :: String
    , id :: String
    , value :: Int
    , onChange :: Int -> Effect Unit
    , min :: Int -- TODO how to declare these optional?
    , max :: Int
    }

numberInput :: NumberInputProps -> React.JSX
numberInput = unsafePerformEffect mkNumberInput

mkNumberInput :: React.Component NumberInputProps
mkNumberInput = React.component "NumberInput" f
  where
  f props =
    do
      pure
      $ R.div
          { className: "number-input"
          , children:
              [ R.label
                  { htmlFor: props.id
                  , children:
                      [ R.text props.label
                      ]
                  }
              , R.input
                  { type: "number"
                  , id: props.id
                  , onChange: RE.handler RDE.targetValue onChange
                  , value: show props.value
                  , max: show props.max
                  , min: show props.min
                  }
              ]
          }
    where
    onChange :: Maybe String -> Effect Unit
    onChange Nothing = pure mempty

    onChange (Just s) = case fromString s of
      Nothing -> pure mempty
      Just n -> props.onChange n
