module Component.Play where

import Prelude
import Data.Set as Set
import Data.Maybe (Maybe(..), maybe)
import Data.Tuple.Nested ((/\))
import Data.Array as Array
import Effect (Effect)
import Effect.Unsafe (unsafePerformEffect)
import React.Basic.Hooks as React
import React.Basic.DOM as R
import React.Basic.Events as RE
import React.Basic.DOM.Events as RDE
import Play (PlayState(..), Cell(..), handleClick, Coord, isFlagged, isBomb, Action(..), allNeighbors)

type Props
  = { state :: PlayState
    , setState :: PlayState -> Effect Unit
    , onReset :: Unit -> Effect Unit
    , onBack :: Unit -> Effect Unit
    }

play :: Props -> React.JSX
play = unsafePerformEffect mkPlay

mkPlay :: React.Component Props
mkPlay = React.component "Play" go
  where
  go props = render
    where
    (PlayState st) = props.state

    allCells = Array.concat st.cells

    flagCount = Array.length $ Array.filter isFlagged $ allCells

    bombCount = Array.length $ Array.filter isBomb $ allCells

    count = bombCount - flagCount

    render = React.do
      clickingCells /\ setClickingCells <- React.useState' Nothing
      let
        onClick :: Coord -> Action -> Effect Unit
        onClick coord action = props.setState (handleClick coord action props.state)

        onClickStart :: Coord -> Action -> Effect Unit
        onClickStart coord action = case action of
          Flag -> pure unit
          Primary -> setClickingCells (Just (Set.singleton coord))
          Zone -> setClickingCells (Just (Set.fromFoldable ([ coord ] <> allNeighbors coord)))

        doRows :: Array (Array Cell) -> Array React.JSX
        doRows rows = Array.mapWithIndex f rows
          where
          f y row =
            R.div
              { className: "row"
              , key: show y
              , children: doRow y row
              }

        doRow :: Int -> Array Cell -> Array React.JSX
        doRow y row = cell <$> Array.mapWithIndex f row
          where
          f x c =
            { key: show x
            , cell: c
            , onClick: onClick { x, y }
            , onClickStart: onClickStart { x, y }
            , clicking: maybe false (Set.member { x, y }) clickingCells
            , dead: st.dead
            }
      pure
        $ R.div
            { className: "play"
            , children:
                [ R.header
                    { children:
                        [ R.div
                            { className: "flags"
                            , children: [ R.text $ show $ count ]
                            }
                        , R.button
                            { className: "back"
                            , onClick: RE.handler RE.syntheticEvent (\_ -> props.onBack unit)
                            , children: [ R.text "back" ]
                            }
                        , R.button
                            { className: "reset"
                            , onClick: RE.handler RE.syntheticEvent (\_ -> props.onReset unit)
                            , children: [ R.text "reset" ]
                            }
                        , R.div
                            { className: "timer"
                            , children: [ R.text "TODO" ]
                            }
                        ]
                    }
                , R.div
                    { className: "grid-container"
                    , children:
                        [ R.div
                            { className: "grid"
                            , children: doRows st.cells
                            , onMouseUp:
                                RE.handler RDE.preventDefault
                                  (\_ -> setClickingCells Nothing)
                            , onMouseOut:
                                RE.handler RDE.preventDefault
                                  (\_ -> setClickingCells Nothing)
                            , onContextMenu:
                                RE.handler RDE.preventDefault
                                  (\_ -> pure unit)
                            }
                        ]
                    }
                ]
            }

type CellProps
  = { cell :: Cell
    , onClickStart :: Action -> Effect Unit
    , onClick :: Action -> Effect Unit
    , dead :: Boolean
    , key :: String
    , clicking :: Boolean
    }

cell :: CellProps -> React.JSX
cell = unsafePerformEffect mkCell

mkCell :: React.Component CellProps
mkCell = React.component "Cell" f
  where
  f props = go
    where
    go = React.do
      text <-
        React.useMemo { dead: props.dead, cell: props.cell }
          ( \_ -> case props.cell of
              ClickedBomb -> show props.cell
              ClickedNeighbor n -> show n
              FlaggedBlank -> show props.cell
              FlaggedBomb -> show props.cell
              FlaggedNeighbor _ -> show props.cell
              HiddenBomb
                | props.dead -> show props.cell
                | otherwise -> ""
              _ -> ""
          )
      extraClassNames <-
        React.useMemo { dead: props.dead, cell: props.cell }
          ( \_ -> case props.cell of
              ClickedBomb -> [ "clicked", "bomb" ]
              ClickedBlank -> [ "clicked" ]
              ClickedNeighbor n -> [ "clicked", "neighbor-" <> show n ]
              FlaggedBomb -> [ "flagged", "bomb" ]
              FlaggedBlank -> [ "flagged" ]
              FlaggedNeighbor _ -> [ "flagged" ]
              HiddenBomb
                | props.dead -> [ "bomb" ]
                | otherwise -> []
              _ -> []
          )
      pure
        $ R.span
            { className:
                Array.intercalate " "
                  $ Array.concat
                      [ [ "cell" ]
                      , if props.clicking then [ "active" ] else []
                      , extraClassNames
                      ]
            , children: [ R.text $ text ]
            , onMouseEnter: mouseHandler RDE.buttons toActionForHover props.onClickStart
            , onMouseDown: mouseHandler RDE.button toAction props.onClickStart
            , onMouseUp: mouseHandler RDE.button toAction props.onClick
            }

  toAction n = case n of
    0 -> Just Primary
    1 -> Just Zone
    2 -> Just Flag
    _ -> Nothing

  -- wtf
  toActionForHover n = case n of
    1 -> Just Primary
    4 -> Just Zone
    _ -> Nothing

  mouseHandler eventFn actionFn effectFn =
    RE.handler
      (eventFn <<< RDE.preventDefault)
      ( \e -> case actionFn =<< e of
          Just action -> effectFn action
          Nothing -> pure unit
      )
