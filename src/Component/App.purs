module Component.App where

import Prelude
import Data.Tuple (Tuple)
import Data.Tuple.Nested ((/\))
import Effect (Effect)
import Effect.Unsafe (unsafePerformEffect)
import React.Basic.Hooks as React
import React.Basic.DOM as R
import InitPlayState (toPlayState)
import Component.Config (config, ConfigState, initConfigState)
import Component.Play (play)
import Play (PlayState(..))

type Props
  = { header :: String
    }

app :: Props -> React.JSX
app = unsafePerformEffect mkApp

data AppState
  = Configuring ConfigState
  | Playing PlayState

mkApp :: React.Component Props
mkApp = do
  React.component "App" \props -> React.do
    stateHook <- React.useState' (Configuring initConfigState)
    pure
      $ R.div
          { className: "app"
          , children:
              [ R.h1
                  { children:
                      [ R.text props.header
                      ]
                  }
              , renderAppState stateHook
              ]
          }

renderAppState :: Tuple AppState (AppState -> Effect Unit) -> React.JSX
renderAppState ((Configuring configState) /\ setState) =
  config
    { state: configState
    , setState: setState <<< Configuring
    , onSubmit
    }
  where
  onSubmit _ = do
    playState <- toPlayState configState
    setState $ Playing playState

renderAppState ((Playing playState) /\ setState) =
  play
    { state: playState
    , setState: setState <<< Playing
    , onBack
    , onReset
    }
  where
  onBack _ = setState $ Configuring configState
    where
    (PlayState { configState }) = playState

  onReset _ = do
    let
      (PlayState { configState }) = playState
    st <- toPlayState configState
    setState $ Playing st
