module Play where

import Prelude
import Data.Array ((!!))
import Data.Array as Array
import Data.Maybe (Maybe(..), fromMaybe)
import Data.Set (Set)
import Data.Set as Set
import Control.Monad.ST (ST, run, while, foreach)
import Control.Monad.ST.Ref (new, modify, read)
import Component.Config (ConfigState)

newtype PlayState
  = PlayState
  { cells :: Array (Array Cell)
  , dead :: Boolean
  , configState :: ConfigState
  }

data Cell
  = HiddenBomb
  | HiddenBlank
  | HiddenNeighbor Int
  | ClickedBomb
  | ClickedBlank
  | ClickedNeighbor Int
  | FlaggedBomb
  | FlaggedBlank
  | FlaggedNeighbor Int

instance showCell :: Show Cell where
  show c = case c of
    HiddenBomb -> "💣"
    HiddenBlank -> " "
    HiddenNeighbor _ -> " "
    ClickedBomb -> "💥"
    ClickedBlank -> "_"
    ClickedNeighbor n -> show n
    FlaggedBomb -> "🚩"
    FlaggedBlank -> "🚩"
    FlaggedNeighbor _ -> "🚩"

derive instance eqCell :: Eq Cell

type Coord
  = { x :: Int, y :: Int }

data Action
  = Primary
  | Flag
  | Zone

handleClick :: Coord -> Action -> PlayState -> PlayState
handleClick coord action (PlayState st)
  | st.dead = PlayState st
  | otherwise =
    fromMaybe (PlayState st) do
      cell <- getCell st.cells coord
      let
        cells = case action of
          Primary -> doClick st.cells coord
          Flag
            | isClicked cell -> st.cells
            | otherwise -> updateAt coord toggleFlag st.cells
          Zone
            | not $ canZoneClick st.cells coord -> st.cells
            | otherwise -> Array.foldl doClick st.cells ([ coord ] <> allNeighbors coord)

        dead = Array.any (\c -> isBomb c && isClicked c) (Array.concat cells)
      pure (PlayState st { cells = cells, dead = dead })

getCell :: Array (Array Cell) -> Coord -> Maybe Cell
getCell cells { x, y } = do
  row <- cells !! y
  row !! x

canZoneClick :: Array (Array Cell) -> Coord -> Boolean
canZoneClick cells coord = case getCell cells coord of
  Just (ClickedNeighbor n) -> n == flaggedNeighbors
    where
    flaggedNeighbors = Array.length (Array.filter isFlagged neighborCells)

    neighborCells = Array.mapMaybe (getCell cells) (allNeighbors coord)
  _ -> false

doClick :: Array (Array Cell) -> Coord -> Array (Array Cell)
doClick cells coord = case getCell cells coord of
  Just HiddenBlank -> floodFill coord cells
  Just HiddenBomb -> setAt coord ClickedBomb cells
  Just (HiddenNeighbor n) -> setAt coord (ClickedNeighbor n) cells
  _ -> cells

updateAt :: Coord -> (Cell -> Cell) -> Array (Array Cell) -> Array (Array Cell)
updateAt { x, y } f cells =
  fromMaybe cells do
    row <- cells !! y
    cell <- row !! x
    newRow <- Array.updateAt x (f cell) row
    Array.updateAt y newRow cells

setAt :: Coord -> Cell -> Array (Array Cell) -> Array (Array Cell)
setAt coord newCell = updateAt coord (const newCell)

isHiddenNeighbor :: Cell -> Boolean
isHiddenNeighbor c = case c of
  HiddenNeighbor _ -> true
  _ -> false

isClicked :: Cell -> Boolean
isClicked c = case c of
  ClickedNeighbor _ -> true
  ClickedBomb -> true
  ClickedBlank -> true
  _ -> false

isFlagged :: Cell -> Boolean
isFlagged c = case c of
  FlaggedNeighbor _ -> true
  FlaggedBomb -> true
  FlaggedBlank -> true
  _ -> false

isBomb :: Cell -> Boolean
isBomb c = case c of
  HiddenBomb -> true
  FlaggedBomb -> true
  ClickedBomb -> true
  _ -> false

unhide :: Cell -> Cell
unhide c = case c of
  HiddenBlank -> ClickedBlank
  HiddenBomb -> ClickedBomb
  (HiddenNeighbor n) -> ClickedNeighbor n
  x -> x

toggleFlag :: Cell -> Cell
toggleFlag c = case c of
  HiddenBlank -> FlaggedBlank
  HiddenBomb -> FlaggedBomb
  (HiddenNeighbor n) -> FlaggedNeighbor n
  FlaggedBlank -> HiddenBlank
  FlaggedBomb -> HiddenBomb
  (FlaggedNeighbor n) -> HiddenNeighbor n
  x -> x

floodFill :: Coord -> Array (Array Cell) -> Array (Array Cell)
floodFill coord cells = case maybeCellType of
  Just HiddenBlank ->
    run do
      let
        region = findRegion (Set.singleton coord)
      newGrid <- new cells
      foreach (Array.fromFoldable region)
        ( \c -> do
            _ <- modify (updateAt c unhide) newGrid
            pure unit
        )
      read newGrid
  _ -> cells
  where
  maybeCellType = getCell cells coord

  findRegion :: Set Coord -> Set Coord
  findRegion cs = run (go cs)
    where
    go :: forall r. Set Coord -> ST r (Set Coord)
    go coords = do
      region <- new coords
      while
        ( do
            nextCoords <- read region
            let
              newMatchingNeighbors = findNewMatchingNeighbors Orthogonal ((==) HiddenBlank) nextCoords
            _ <- modify (Set.union newMatchingNeighbors) region
            pure ((Set.size newMatchingNeighbors) /= 0)
        )
        (pure unit)
      blanks <- read region
      let
        neighbors = findNewMatchingNeighbors All isHiddenNeighbor blanks
      pure $ Set.union blanks neighbors

    findNewMatchingNeighbors :: NeighborType -> (Cell -> Boolean) -> Set Coord -> Set Coord
    findNewMatchingNeighbors neighborType test coords = neighbors
      where
      neighbors = Set.unions (Set.map findCellNeighbors coords)

      findCellNeighbors c =
        Set.fromFoldable
          (Array.filter isValid (neighborFn c))

      neighborFn = case neighborType of
        Orthogonal -> orthogonalNeighbors
        All -> allNeighbors

      isValid :: Coord -> Boolean
      isValid c = isNew c && passesTest c

      isNew :: Coord -> Boolean
      isNew c = not Set.member c coords

      passesTest :: Coord -> Boolean
      passesTest { x, y } = case getCell cells { x, y } of
        Nothing -> false
        Just cell -> test cell

data NeighborType
  = Orthogonal
  | All

orthogonalNeighbors :: Coord -> Array Coord
orthogonalNeighbors { x, y } =
  [ { x: x + 1, y }
  , { x: x - 1, y }
  , { x, y: y + 1 }
  , { x, y: y - 1 }
  ]

allNeighbors :: Coord -> Array Coord
allNeighbors { x, y } =
  [ { x: x + 1, y }
  , { x: x + 1, y: y + 1 }
  , { x: x + 1, y: y - 1 }
  , { x: x - 1, y }
  , { x: x - 1, y: y + 1 }
  , { x: x - 1, y: y - 1 }
  , { x: x, y: y + 1 }
  , { x: x, y: y - 1 }
  ]
