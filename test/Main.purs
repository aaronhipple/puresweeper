module Test.Main where

import Prelude
import Data.Array as Array
import Effect (Effect)
import Effect.Aff (launchAff_)
import Test.Spec (describe, it)
import Test.Spec.Assertions (shouldEqual)
import Test.Spec.Reporter.Console (consoleReporter)
import Test.Spec.Runner (runSpec)
import Play (floodFill, Cell(..))

main :: Effect Unit
main =
  launchAff_
    $ runSpec [ consoleReporter ] do
        describe "Component.Play" do
          describe "floodFill" do
            it "should fill a region" do
              let
                cells =
                  [ [ HiddenBlank, HiddenBlank, HiddenBlank ]
                  , [ HiddenBlank, HiddenBlank, HiddenBlank ]
                  , [ HiddenBlank, HiddenBlank, HiddenBlank ]
                  ]

                expected =
                  [ [ ClickedBlank, ClickedBlank, ClickedBlank ]
                  , [ ClickedBlank, ClickedBlank, ClickedBlank ]
                  , [ ClickedBlank, ClickedBlank, ClickedBlank ]
                  ]

                coord = { x: 1, y: 1 }
              floodFill coord cells `shouldEqual` expected
            it "should fill a region up to the first rank of neighbor cells (inclusive)" do
              let
                cells =
                  [ [ HiddenBlank, HiddenBlank, HiddenBlank, HiddenBlank, HiddenBlank ]
                  , [ HiddenBlank, HiddenBlank, HiddenBlank, HiddenBlank, HiddenBlank ]
                  , [ HiddenBlank, HiddenBlank, HiddenBlank, HiddenBlank, HiddenNeighbor 1 ]
                  , [ HiddenBlank, HiddenBlank, HiddenBlank, HiddenNeighbor 2, HiddenBomb ]
                  , [ HiddenBlank, HiddenBlank, HiddenNeighbor 1, HiddenBomb, HiddenNeighbor 2 ]
                  ]

                expected =
                  [ [ ClickedBlank, ClickedBlank, ClickedBlank, ClickedBlank, ClickedBlank ]
                  , [ ClickedBlank, ClickedBlank, ClickedBlank, ClickedBlank, ClickedBlank ]
                  , [ ClickedBlank, ClickedBlank, ClickedBlank, ClickedBlank, ClickedNeighbor 1 ]
                  , [ ClickedBlank, ClickedBlank, ClickedBlank, ClickedNeighbor 2, HiddenBomb ]
                  , [ ClickedBlank, ClickedBlank, ClickedNeighbor 1, HiddenBomb, HiddenNeighbor 2 ]
                  ]

                coord = { x: 0, y: 0 }
              floodFill coord cells `shouldEqual` expected
            it "should fill a region up to the first rank of neighbor cells (inclusive, non-orthogonal)" do
              let
                cells =
                  [ [ HiddenBlank, HiddenBlank, HiddenBlank, HiddenBlank, HiddenBlank ]
                  , [ HiddenBlank, HiddenBlank, HiddenBlank, HiddenBlank, HiddenBlank ]
                  , [ HiddenBlank, HiddenBlank, HiddenNeighbor 1, HiddenNeighbor 2, HiddenNeighbor 1 ]
                  , [ HiddenBlank, HiddenNeighbor 1, HiddenBomb, HiddenNeighbor 3, HiddenBomb ]
                  , [ HiddenBlank, HiddenNeighbor 1, HiddenNeighbor 2, HiddenBomb, HiddenNeighbor 2 ]
                  ]

                expected =
                  [ [ ClickedBlank, ClickedBlank, ClickedBlank, ClickedBlank, ClickedBlank ]
                  , [ ClickedBlank, ClickedBlank, ClickedBlank, ClickedBlank, ClickedBlank ]
                  , [ ClickedBlank, ClickedBlank, ClickedNeighbor 1, ClickedNeighbor 2, ClickedNeighbor 1 ]
                  , [ ClickedBlank, ClickedNeighbor 1, HiddenBomb, HiddenNeighbor 3, HiddenBomb ]
                  , [ ClickedBlank, ClickedNeighbor 1, HiddenNeighbor 2, HiddenBomb, HiddenNeighbor 2 ]
                  ]

                coord = { x: 0, y: 0 }
              floodFill coord cells `shouldEqual` expected
            it "should fill a 30x30 region in a reasonable amount of time" do
              let
                cells = Array.replicate 30 (Array.replicate 30 HiddenBlank)

                expected = Array.replicate 30 (Array.replicate 30 ClickedBlank)

                coord = { x: 0, y: 0 }
              floodFill coord cells `shouldEqual` expected
