module InitPlayState where

import Prelude
import Control.Monad.Trans.Class (lift)
import Control.Monad.State.Trans (execStateT, StateT, get, modify)
import Control.Monad.Loops (whileM_)
import Data.Array ((..))
import Data.Set (Set)
import Data.Set as Set
import Effect (Effect)
import Effect.Random (randomInt)
import Component.Config (ConfigState(..))
import Play (PlayState(..), Cell(..), Coord, allNeighbors)

toPlayState :: ConfigState -> Effect PlayState
toPlayState configState@(ConfigState st) = do
  coords <- randomCoords st
  let
    cells = (map $ toCells coords) <$> (allCoords st)
  pure $ PlayState { configState, cells, dead: false }

allCoords :: forall r. { rows :: Int, cols :: Int | r } -> Array (Array Coord)
allCoords { rows, cols } = do
  x <- 0 .. (cols - 1)
  pure
    $ do
        y <- 0 .. (rows - 1)
        pure { x, y }

toCells :: Set Coord -> Coord -> Cell
toCells bombs coord
  | Set.member coord bombs = HiddenBomb
  | otherwise = cell
    where
    cell
      | countNeighbors == 0 = HiddenBlank
      | otherwise = HiddenNeighbor countNeighbors

    neighbors = Set.fromFoldable (allNeighbors coord)

    countNeighbors = Set.size (Set.intersection bombs neighbors)

randomCoords :: forall r. { rows :: Int, cols :: Int, bombs :: Int | r } -> Effect (Set Coord)
randomCoords configState@{ bombs } = execStateT m Set.empty
  where
  m :: StateT (Set Coord) Effect Unit
  m =
    whileM_
      ( do
          s <- get
          pure (Set.size s < bombs)
      )
      ( do
          c <- lift $ randomCoord configState
          modify (Set.insert c)
      )

randomCoord :: forall r. { rows :: Int, cols :: Int | r } -> Effect Coord
randomCoord { rows, cols } = do
  x <- randomInt 0 (cols - 1)
  y <- randomInt 0 (rows - 1)
  pure { x, y }
